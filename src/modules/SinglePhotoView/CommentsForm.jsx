import {useState} from 'react'

function CommentsForm({onSubmit}) {
    let [username, setUsername] = useState('')
    let [comment, setComment] = useState('')

    return <div className='commentsForm'>
        <input type="text" required onChange={e => setUsername(e.nativeEvent.target.value)} value={username}
               placeholder='Ваше имя'/>
        <input type="text" required onChange={e => setComment(e.nativeEvent.target.value)} value={comment}
               placeholder='Ваш комментарий'/>
        <input type="submit"
               onClick={() => {
                   setUsername('');
                   setComment('');
                   onSubmit([username, comment])
               }
               } value='Оставить комментарий' disabled={!(username && comment)}/>
    </div>
}

export default CommentsForm