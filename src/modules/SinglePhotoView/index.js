import {useState, useEffect} from 'react'
import CommentsForm from './CommentsForm'
import './index.css'


function SinglePhotoView({photoId, imagesHost, commentsHost}) {
    let [imageUrl, setImageUrl] = useState(null)
    let [comments, setComments] = useState([])


    useEffect(() => {
        fetch(imagesHost + '/' + photoId)
            .then((response) => {
                return response.json();
            })
            .then(imageData => setImageUrl(imageData.src))
            .catch(e => console.error('Error fetching image ' + photoId + ' :', e))
    }, [])

    useEffect(() => {
        fetch(commentsHost + '/' + photoId)
            .then((response) => {
                return response.json();
            })
            .then(comments => setComments(comments))
            .catch(e => console.error('Error fetching comments ' + photoId + ' :', e))
    }, [])

    const handleCommentsAdd = ([username, comment]) => {

        const payload = {
            "name": username,
            "description": comment,
            "image_id": photoId
        }

        fetch('https://tzfrontend.herokuapp.com/comments/add/',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(payload)
            }
            )
            .then(responce => {
                if(responce.ok) {
                    comments.push(payload)
                    setComments([...comments])
                }
            })
            .catch((res) => { console.error('Error posting comment', res) })
    }

    console.log('---comments',comments)

    return (
        <div className='singlePhotoView'>
            <div className='imageAndCommentsWrapper'>
                <div className='image'><img src={imageUrl}/></div>
                <ul className='comments'>
                    {comments.map(comment => <li><div className='name'>{comment.name}</div> <div className='comment'>{comment.description}</div></li>)}
                </ul>
            </div>

            <CommentsForm onSubmit={handleCommentsAdd}/>
        </div>

    )
}

export default SinglePhotoView