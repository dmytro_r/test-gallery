import React, {useState, useEffect, useCallback} from 'react'
import Modal from "../Modal";
import SinglePhotoView from '../SinglePhotoView'
import './index.css'

function Gallery({imagesListUrl, enlargedImageUrl}) {
    let [imagesList, setImagesList] = useState([]);
    useEffect(() => {
        fetch(imagesListUrl)
            .then((response) => {
            return response.json();
        })
            .then(imagesList => setImagesList(imagesList))
            .catch(e => console.error('Error fetching Gallery images:', e))
    }, [])
    let [openImageId, setOpenImageId] = useState();

    let handleImageClick = (imageId) => () => {
        setOpenImageId(imageId)
    }
    let closeModal = useCallback( () => {setOpenImageId(null)} )

    return <div className='imagesContainer'>
        {imagesList.map( image => <img src={image.src} onClick={handleImageClick(image.image_id)}/>)}
        {openImageId &&
        <Modal onClose={closeModal}>
            <SinglePhotoView
                imagesHost='https://tzfrontend.herokuapp.com/images'
                commentsHost='https://tzfrontend.herokuapp.com/comments'
                photoId={openImageId}/>
        </Modal>}
    </div>;
}

export default Gallery