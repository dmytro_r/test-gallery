import logo from './logo.svg';
import './App.css';
import Gallery from './modules/Gallery'

function App() {
    return (
        <div className="App">
            <header>
                Test App
            </header>
            <section className='main'>
                <Gallery imagesListUrl='https://tzfrontend.herokuapp.com/images/' />
            </section>

            <footer>
                 &copy; 2018-2019
            </footer>
        </div>
    );
}

export default App;
